#ifndef CONTANT_H
    #define CONTANT_H


//the maximum length of string
#define MAX_LENGTH 256
//message when we can't read file
//this should be used in printf or printf-like function
#define UNABLE_READ_FILE(filename) "ERROR! You have no permission to read %s or file is not exist!\n", filename

#define TRUE 1
#define FALSE 0

#endif